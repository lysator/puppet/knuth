# NGNINX configuration for Lysators webserver.
# Most servers and locations simply forward calls to Roxen
class knuth::nginx {

  $letsencrypt_certname = 'lysator-cert'

  knuth::certbot { $letsencrypt_certname:
    domains => [
      'www.lysator.se',
      'lysator.se',
      'planet.lysator.liu.se',
      'knuth.lysator.liu.se',
      ],
  }


  # cat /etc/ssl/www/{www.lysator.liu.se,DigiCertCA}.crt > /etc/ssl/www/www.lysator.liu.se.chain.crt

  class { '::nginx':
    gzip         => 'on',
    server_purge => true,
    log_format   =>  { 'lyslog' => @(EOT/L)
      $remote_addr - $remote_user [$time_iso8601] \
      $request_method $scheme://$host$request_uri $status $body_bytes_sent \
      "$http_referer" "$http_user_agent" "$gzip_ratio"
      |- EOT
    },
  }

  $fincert = false

  if ($fincert) {
    $liucert = {
      ssl_cert         => '/etc/ssl/www/www.lysator.liu.se.chain.crt',
      ssl_key          => '/etc/ssl/www/www.lysator.liu.se.key',
      ssl_trusted_cert => '/etc/ssl/www/DigiCertCA.crt',
    }
  } else {
    $not_quite_fincert = 'lysator-www-cert'
    knuth::certbot { $not_quite_fincert:
      domains => [
        'lysator.liu.se',
        'www.lysator.liu.se',
      ],
    }
    $liucert = {
      ssl_cert => "/etc/letsencrypt/live/${not_quite_fincert}/fullchain.pem",
      ssl_key  => "/etc/letsencrypt/live/${not_quite_fincert}/privkey.pem",
    }
  }

  $letsencryptcert = {
    ssl_cert => "/etc/letsencrypt/live/${letsencrypt_certname}/fullchain.pem",
    ssl_key  => "/etc/letsencrypt/live/${letsencrypt_certname}/privkey.pem",
  }

  $nginx_default = {
    ensure               => present,
    ipv6_enable          => true,
    ipv6_listen_options  => '',
    ssl                  => true,
    ssl_redirect         => true,
    ssl_protocols	 => 'TLSv1.2 TLSv1.3',
    ssl_prefer_server_ciphers => 'off',
    ssl_ciphers => 'ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-CHACHA20-POLY1305',
    use_default_location => false,
    listen_options       => '',
    access_log           => '/var/log/nginx/lysator_access.log',
    error_log            => '/var/log/nginx/lysator_error.log',
    format_log           => 'lyslog',
  }

  nginx::resource::server {
  default: * => $nginx_default ;
  'www.lysator.se':
    server_name => ['www.lysator.se', 'lysator.se'],
    *           => $letsencryptcert,
    ;
  'lysator.liu.se':
    * => $liucert,
    ;
  }

  nginx::resource::location { 'lysator redirect':
    server              => ['www.lysator.se', 'lysator.liu.se'],
    location            => '/',
    ssl                 => true,
    ssl_only            => true,
    location_cfg_append => {
      'rewrite' => '^/(.*)$ $scheme://www.lysator.liu.se/$1 permanent'
    },
  }


  nginx::resource::server {
  default: * => $nginx_default ;
  'www.lysator.liu.se':
    listen_options    => 'default_server',
    *                 => $liucert,
    ssl_redirect      => false,
    server_cfg_append => {
      'charset'    => 'utf-8',

      'add_header' => [ 'Struct-Transport-Security "max-age=3600"', ],

      # Fix av saknade trailing slash. Ännu en del av våran legacy,
      # men var lika bra att behålla.
      'rewrite'    =>
        # Gamla omskrivningsregler tagna från nyarlathotep.
        # Många av de här skulle egentligen bara kunna slängas. Men
        # jag orkar inte kolla huruvida de fortfarande är i bruk.
        [ '^/rag(?:/(.*))?$        http://rag.lysator.liu.se/$1        permanent',
          '^/studieinfo(?:/(.*))?$ http://studieinfo.lysator.liu.se/$1 permanent',
          '^/ftp(?:/(.*))?$        $scheme://ftp.lysator.liu.se/$1     permanent',

          '^/local/datorhandbok/(.*)$ http://datorhandbok.lysator.liu.se/index.php?search=$1 permanent',
          '^/local/datorhandbok$      http://datorhandbok.lysator.liu.se                     permanent',

          '^/local/(lib|kq)(?:/(.*))?$  /foreningen/lokaler/lib/$1 permanent',
          '^/local/protokoll(?:/(.*))?$ /protokoll/$1              permanent',
          '^/local/lystring/(?:/(.*))?$ /lystring/$1               permanent',
          '^/local/styrdok/(?:/(.*))?$  /foreningen/$1             permanent',
          '^/local/(?:/(.*))?$          /$1                        permanent',

          '^/runeberg(?:/(.*))?$        http://runeberg.org/$1     permanent',
          '^/runeberg/nf/a([a-z])(.*).html$ /runeberg/nfa$1/$2.html',

          '^/users/(.*) /~$1 permanent',
          '^/~/(.+)$    /~$1 permanent',
        ]
    }
    # error_pages => {
    #   404       => '/404.html',
    # }
  }

  $proxies = {
    '/'        => 'http://localhost:8080', # Send to Roxen
    '/foo/'    => 'http://gibraltar:8081',
  }

  $proxies.each |$location, $target| {
    nginx::resource::location{ $location:
      server         => 'www.lysator.liu.se',
      ssl            => true,
      proxy          => $target,
      # Roxen generates http headers when redirecting, since we work
      # unencrypted internally. It does however answer with
      # www.lysator.liu.se (first instance). Second instance of
      # www.lysator.liu.se is our external server name.
      proxy_redirect => 'http://www.lysator.liu.se $scheme://www.lysator.liu.se',
    }
  }

  nginx::resource::location { '/static-test':
    server  => 'www.lysator.liu.se',
    ssl     => true,
    www_root => '/srv/www/static',
  }

  # Not really neeed, but peoples automatic scanners complain, and
  # try to help us by alerting us about it.
  nginx::resource::location { 'blacklist git':
    server              => 'www.lysator.liu.se',
    ssl                 => true,
    location            => '/.git',
    location_cfg_append => {
      'return'          => '403 Forbidden.',
    }
  }

  # nginx::resource::location { 'knuth-redirect':
  #   server              => 'knuth',
  #   location            => '/',
  #   location_cfg_append => {
  #     'rewrite' => '^/(.*)$ https://knuth.lysator.liu.se/$1 permanent'
  #   },
  # }

  # roxen admin interface on knuth
  nginx::resource::server {
  default: * => $nginx_default ;
  # 'knuth': ;
  'knuth.lysator.liu.se':
    * => $letsencryptcert,
    ;
  }

  nginx::resource::location { 'knuth-roxen-proxy':
    server   => 'knuth.lysator.liu.se',
    location => '/',
    ssl      => true,
    ssl_only => true,
    proxy    => 'http://localhost:25980',
  }

  #

  nginx::resource::location { '= /nginx_status':
    server              => 'www.lysator.liu.se',
    location_cfg_append => {
      'stub_status' => 'on',
      'access_log'  => 'off',
      'allow'       => ['130.236.0.0/16', '2001:6b0:17:f0a0::/64'],
      'deny'        => 'all',
    },
  }

  nginx::resource::server {
    default: * => $nginx_default ;
    'planet.lysator.liu.se':
      server_name  => ['planet', 'planet.lysator.liu.se'],
      *            => $letsencryptcert,
      ssl_redirect => false,
      ;
  }

  nginx::resource::location { 'planet-root':
    server              => 'planet.lysator.liu.se',
    location            => '/',
    ssl                 => true,
    location_cfg_append => {
      'rewrite' => '^/(.*)$ https://www.lysator.liu.se/planet/$1 permanent'
    }
  }


  # TODO document these "redirects"
  nginx::resource::server {
    default: * => $nginx_default ;
    'userdir':
      # TODO possibly generate wildcard cert for user domains
      ssl          => false,
      ssl_redirect => false,
      server_name  => [
        '~^(?P<uname>[a-z][-a-z0-9]*)\.www.users.lysator.liu.se',
        '~^(?P<uname>[a-z][-a-z0-9]*)\.user.lysator.liu.se',
        ],
      use_default_location => false,
      access_log           => '/tmp/userdir_access.log',
      error_log            => '/tmp/userdir_error.log',
      ;
  }

  nginx::resource::location { 'userdir-root':
    server           => 'userdir',
    location         => '/',
    proxy            => 'http://localhost:8080/~$uname$request_uri',
    proxy_set_header => [ 'Host $host', ],
    proxy_redirect   => '$scheme://$host/~$uname/ /',
  }

  nginx::resource::server {
  default: * => $nginx_default ;
  'lsff':
    ssl                  => false,
    ssl_redirect         => false,
    server_name          => ['www.lsff.nu', 'lsff.nu'],
    ipv6_enable          => true,
    ipv6_listen_options  => '',
    use_default_location => false,
    listen_port          => 80,
    access_log           => '/var/log/nginx/lsff_access.log',
    error_log            => '/var/log/nginx/lsff_error.log',
    ;
  }

  nginx::resource::location { 'lsff-root':
    server   => 'lsff',
    location => '/',
    proxy    => 'http://localhost:8080/lsff/',
  }
}
