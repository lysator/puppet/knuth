# Sets up environment for knuth::certbot
class knuth::certbot_init (
  String $email = 'www@lysator.liu.se',
) {

  # TODO Better formalization of what the certbot-nginx package is
  # called
  if ($facts['os']['name'] == 'CentOS') {
    if ($facts['os']['release']['major'] == '7') {
      $certbot_nginx = 'python2-certbot-nginx'
    }
  } elsif ($facts['os']['name'] == 'Rocky') {
    if ($facts['os']['release']['major'] == '8') {
      $certbot_nginx = 'python3-certbot-nginx'
    }
  }

  ensure_packages ( [$certbot_nginx,], {
    ensure => installed,
  })

  class { '::letsencrypt':
    config  => {
      email => $email,
    }
  }

  # crond apparently doesn't have /usr/sbin in its path, which is
  # where nginx is located on RedHat systems. This is a dirty fix to
  # allow it to find nginx.
  file { '/bin/nginx':
    ensure => link,
    target => '/usr/sbin/nginx',
  }
}
