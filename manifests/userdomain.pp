# Separate entry-point. This configures a server which anyone can
# point their DNS-records to, and it resolves it to sub-directories on
# Lysators webserver.
#
# Currently all components of the system (except for the actuall
# webcontent) are grouped together here, but it could be split into
# multiple classes and machines.
#
# This should preferably run on a different machine than the main web
# server, to ensure that a bad user-conf doesn't kill the main server.
#
# The system consists of a number of components:
# - A database which contains registered domains, and if they are
#   ready for TLS
# - A script which periodically attempts to fetch data from the
#   registered urls which should only be present on the webserver, and
#   if so flags them that they are TLS enabled. This is needed since
#   failed lets-encrypt-challanges fail whole puppet runs, and they
#   also hang the process.
# - A webbserver which reverse-proxies all domains, and handles their
#   TLS certificates.
class knuth::userdomain {

  file_line { 'disable selinux':
    ensure => present,
    path   => '/etc/selinux/config',
    line   => 'SELINUX=disabled',
    match  => '^SELINUX=',
  }



  package { [
    'mariadb',
    'mariadb-server',
    'python3-sqlalchemy',
  ]:
    ensure => installed,
  }

  service { 'mariadb':
    ensure => 'running',
    enable => true,
  }

  ini_setting { 'mysql bind':
    ensure  => present,
    path    => '/etc/my.cnf.d/server.cnf',
    section => 'mysqld',
    setting => 'bind-address',
    value   => '*',
    notify  => Service['mariadb'],
  }

  include ::lysnetwork::iptables_default_deny

  firewall {
    default:
      chain  => 'INPUT',
      jump => 'accept',
      proto  => 'tcp',
      ;
    '100 WWW IPv4':
      dport    => ['80', '443'],
      protocol => 'iptables',
      ;
    '100 WWW IPv6':
      dport    => ['80', '443'],
      protocol => 'ip6tables',
      ;
    '101 MySQL IPv4':
      source => '130.236.254.0/24',
      dport  => ['3306'],
      ;
    '101 MySQL IPv6':
      source   => '2001:6b0:17:f0a0::/64',
      dport    => ['3306'],
      protocol => 'ip6tables',
      ;
  }

  # Sql database and user setup still manual, but create something
  # like this. Also see files/setup_mysql.sql
  $mysql_users = [{
    'name'     => 'test',
    'host'     => 'admin.lysator.liu.se',
    'password' => 'password',
    'grant'    => [ 'ALL', 'wwwconf.userdomains' ]
  }]

  $userdomains = $facts['userdomains']
  $domains = if ($userdomains == undef) {
    warning('Userdomain fact is null')
    []
  } else {
    $userdomains
  }

  class { '::nginx':
    # We want to purge, to propagate removals from the database to the
    # server config. However, if we lose our database connection it's
    # better to keep the current config.
    # server_purge => $userdomains != null,
    server_purge => true,
    log_format   =>  { 'lyslog' => @(EOT/L)
      $remote_addr - $remote_user [$time_iso8601] \
      $request_method $scheme://$host$request_uri $status $body_bytes_sent \
      "$http_referer" "$http_user_agent" "$gzip_ratio"
      |- EOT
    },
  }

  ### Set up default page as information for user-domains
  # Note that the actuall pages aren't managed here

  $default_webroot = '/var/www'
  $default_cert = 'donald'

  file { $default_webroot:
    ensure => directory,
  }

  knuth::certbot { $default_cert:
    domains => [
      'donald.lysator.liu.se',
      'revproxy.www.lysator.liu.se',
    ],
  }

  $default_ssl = if $facts['letsencrypt_directory']['donald.lysator.liu.se'] {
    {
      ssl      => true,
      ssl_cert => "${facts['letsencrypt_directory']['donald.lysator.liu.se']}/fullchain.pem",
      ssl_key  => "${facts['letsencrypt_directory']['donald.lysator.liu.se']}/privkey.pem",
    }
  } else {
    {
      ssl => false,
    }
  }

  nginx::resource::server { 'default_server':
    ensure              => present,
    ipv6_enable         => true,
    ipv6_listen_options => 'default_server',
    listen_options      => 'default_server',
    ssl_redirect        => false,
    access_log          => '/var/log/nginx/default.log',
    error_log           => '/var/log/nginx/default_error.log',
    www_root            => $default_webroot,
    try_files           => ['$uri', '$uri/', '=404'],
    error_pages         => { '404' => '/404.html', },
    *                   => $default_ssl,
  }

  $fcgi_sock = '/run/fcgiwrap/fcgiwrap-nginx.sock'

  ensure_packages([
    'fcgiwrap',
    'httpd-tools', # for htpasswd command
    ])


  service { 'fcgiwrap@nginx.socket':
    ensure => running,
    enable => true,
  }

  nginx::resource::location { 'blacklist git':
    server              => 'default_server',
    ssl                 => true,
    location            => '/.git',
    location_cfg_append => {
      'return'          => '403 Forbidden.',
    }
  }

  nginx::resource::location {
  default:
    server               => 'default_server',
    ssl                  => true,
    ssl_only             => true,
    auth_basic           => 'Admin area',
    # NOTE this file isn't created automatically
    # Update it by running `htpasswd /etc/nginx/htpasswd <user>`
    auth_basic_user_file => '/etc/nginx/htpasswd',
    # Needs to be set for each location due to bad puppet module...
    www_root             => $default_webroot,
    ;
  '~ \.py':
    fastcgi       => "unix:${fcgi_sock}",
    fastcgi_param => {
      'PATH_INFO'    => '$fastcgi_script_name',
      'QUERY_STRING' => '$args',
    },
    ;
  '/admin':
    index_files => ['index.py'],
    ;
  }

  ### Load userdomains

  create_resources(knuth::userdomain_single,
    $domains.reduce({}) |$hash, $dict| {
      $domain = $dict['domain']
      $user = $dict['user']
      $location = $dict['location']
      $tls_ready = $dict['tls_ready'] == 1
      $hash + {
        "${user} - ${domain} - ${location}"
                    => {
        'domain'    => $domain,
        'user'      => $user,
        'location'  => $location,
        'tls_ready' => $tls_ready,
      }}
    }
  )


  ##################################################


  file { '/var/validator_server':
    ensure => directory,
  }
  file { '/var/validator_server/common_secret.txt':
    ensure  => file,
    content => '14f68f96-34f7-11ec-a1ae-009c0299e47c',
  }

  # Dependency of mysql-connector-python3
  ensure_packages(['python3-protobuf'], {
    ensure => installed,
  })

  # MySQL-connector is missing from rocky-linux's repos, so just
  # download it manually.
  package { 'mysql-connector-python3':
    ensure          => installed,
    provider        => rpm,
    source          => 'https://dev.mysql.com/get/Downloads/Connector-Python/mysql-connector-python3-8.0.27-1.el8.x86_64.rpm',
    install_options => '--nosignature',
  }

  inifile::create_ini_settings({
    'mysql'      => {
      'user'     => 'root',
      'password' => '',
      'host'     => 'localhost',
      'database' => 'wwwconf',
    }}, {
      path => '/etc/update_userdomain_tls.ini',
    })

  file { '/usr/libexec/update_userdomain_tls':
    ensure => file,
    mode   => '0555',
    source => "puppet:///modules/${module_name}/update_userdomain_tls",
  }

  systemd::timer { 'update_userdomain_tls.timer':
    timer_source   => "puppet:///modules/${module_name}/update_userdomain_tls.timer",
    service_source => "puppet:///modules/${module_name}/update_userdomain_tls.service",
    active         => true,
    enable         => true,
  }

}
