# Sets up the Roxen web server.
# Much of what is done here ought to be handled by a system (rpm)
# package, but nobody has bothered to package it that way yet.
# TODO actually download and install the server
# TODO apply our patches
class knuth::roxen {
  # http://download.roxen.com/6.1/
  # Roxen requirements
  package { [
    'patch',
    'gmp',
    'libjpeg-turbo',
    'libaio',
    'libgsf',
  ]:
    ensure => installed,
  }


  # "http://download.roxen.com/6.1/dl.html/roxen-6.1.246-r5-ws-rhel7_x86_64.sh"

  include 'systemd'
  systemd::unit_file { 'roxen.service':
    content => file('knuth/roxen.service'),
  }

  # roxen requires a mysql server, but it provides its own, which we
  # just let it have
  service { 'roxen':
    enable => true,
  }

#   root@roxen:25640
# download
# ./script --install=roxen --prefix=/usr/local
}
