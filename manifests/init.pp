# General setup for Lysators WWW-server
# The main webserver is a Roxen web server, which is fronted by an
# NGNIX server since that's easier to have correctly configured, and
# solves a number of weird cases we have accumelated over the years.
#
# Roxen exports two file trees.
# - root under www.lysator.liu.se/, which is probably located in
#   /srv/www/roxen on the webserver, and exported to
#   /lysator/www/roxen on all Lysator workstations.
# - All users webdirectories, under www.lysator.liu.se/~<user>,
#   which probably is /srv/www/users/<user>, which is found at
#   /lysator/www/users/<user> on workstations.
#
# Nginx handles all TLS, and internally Nginx and Roxen speek
# unencrypted.
#
# This file is mostly for the general config, see other files for
# actual webserver config.
class knuth {

  file_line { 'disable selinux':
    ensure => present,
    path   => '/etc/selinux/config',
    line   => 'SELINUX=disabled',
    match  => '^SELINUX=',
  }

  ssh_authorized_key { 'root@nis.lysator.liu.se':
    ensure => present,
    user   => 'root',
    type   => 'ssh-ed25519',
    key    => 'AAAAC3NzaC1lZDI1NTE5AAAAILjOLZlLhu2a/yG7b9y+EUdvjvnwexuKsgT6E4+VWlaL',
  }

  package { [ 'nfs-utils' ]:
    ensure => installed,
  }

  mount { '/home':
    ensure => mounted,
    device => 'home:/tank/users',
    fstype => 'nfs',
  }

  mount { '/srv':
    ensure => mounted,
    device => '/dev/www/lvol0',
    fstype => 'ext4',
  } -> file { '/srv/www/roxen':
    ensure => directory,
    # sticky, setgid
    mode   => '3775',
    group  => 'www',
  } -> file { '/srv/www/users':
    ensure => directory,
  }

  file { '/etc/exports':
    ensure => present
  } -> file_line { 'Webdir export':
    ensure => present,
    path   => '/etc/exports',
    line   => '/srv/www @lysnet(rw,sync,no_subtree_check)',
  }


  ['tcp', 'udp'].each |$proto| {
    firewall {
      default:
        proto  => $proto,
        dport  => [111, 20048, 2049], # 'portmapper', 'mountd', 'nfs'
        jump => 'accept', ;
      "050 Accept mounts ${proto} from Lysator":
        source => '130.236.254.0/24', ;
      "051 Accept mounts ${proto} from Lysator":
        source => '130.236.253.0/24', ;
      "050 Accept mounts ${proto} from Lysator IPv6":
        source   => '2001:6b0:17:f0a0::0/64',
        protocol => 'ip6tables', ;
    }
  }


  service { 'nfs':
    ensure => 'running',
    enable => true,
  }

  file { '/etc/logrotate.d/roxen':
    ensure => present,
    source => 'puppet:///modules/knuth/logrotate.conf',
  }
  file { '/etc/logrotate.conf':
    ensure => present,
  }
  cron { 'logrotate':
    command => '/usr/sbin/logrotate /etc/logrotate.d/roxen',
    minute  => 0,
    hour    => 4,
  }

  package { 'python':
    ensure => installed,
  }
  cron { 'planetlysator':
    command => '/srv/www/roxen/planet/planetrunner/planetrunner.py',
    user    => 'planet',
    minute  => [0, 30],
  }

  file { '/opt/lysator-scripts':
    ensure => directory,
    mode   => '0755',
  }
  -> file { '/opt/lysator-scripts/gen_webdir.knuth':
    content => file('knuth/gen_webdir.knuth'),
    mode    => '0755',
  }

  include ::lysnetwork::iptables_default_deny

  firewall {
    default:
      chain  => 'INPUT',
      jump => 'accept',
      dport  => ['80', '443'],
      proto  => 'tcp',
      ;
    '100 Roxen':
      ;
    '100 Roxen IPv6':
      protocol => 'ip6tables',
      ;
    '100 Userdomain host':
      dport  => 8080,
      source => '130.236.254.24',
      ;
    '100 Userdomain host IPv6':
      protocol => 'ip6tables',
      dport    => 8080,
      source   => '2001:6b0:17:f0a0::18',
      ;
  }

  class { 'lysbackup':
    backup_user => 'knuth',
    exclude     => [ '/home', ],
  }

  user { 'planet':
    ensure     => present,
    allowdupe  => true,
    forcelocal => true,
    uid        => 10002,
    home       => '/srv/www/roxen/planet',
  }

  include ::knuth::roxen
  include ::knuth::nginx

  # include ::knuth::userdomain
}
