# letsencrypt::certbot but with sensible options.
# Alos automatically sets up its own environment
define knuth::certbot (
  Array[String] $domains,
  Enum['present','absent'] $ensure = 'present',
  String $letsencrypt_certname = $name
) {

  require ::knuth::certbot_init

  if ($ensure == 'present')  {
    letsencrypt::certonly { $letsencrypt_certname:
      ensure             => present,
      domains            => $domains,
      manage_cron        => true,
      plugin             => 'nginx',
      additional_args    => [ '--quiet', ],
      post_hook_commands => [ 'systemctl reload nginx.service', ],
    }
  } else {
    letsencrypt::certonly { $letsencrypt_certname:
      ensure             => absent,
      domains            => $domains,
      manage_cron        => true,
    }
  }
}
