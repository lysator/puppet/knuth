# Configures a single userdomain
# Instanciated by knuth::userdomain
define knuth::userdomain_single (
  String $domain,
  String $user,
  String $location = '/',
  Boolean $tls_ready = false,
) {

  $certname = "userdomain_cert_${domain}"

  $tls_really_ready = $tls_ready and $domain in $facts['letsencrypt_directory']

  if ($tls_ready) {
    knuth::certbot { $certname:
      ensure  => present,
      domains => [ $domain ],
    }
    $ssl_cert = "/etc/letsencrypt/live/${certname}/fullchain.pem"
    $ssl_key  = "/etc/letsencrypt/live/${certname}/privkey.pem"
  } else {
    $ssl_cert = false
    $ssl_key  = false
    knuth::certbot { $certname:
      ensure  => absent,
      domains => [ $domain ],
    }
  }

  nginx::resource::server { "userdomain_server_${user}_${domain}":
    server_name          => [ $domain ],
    ipv6_enable          => true,
    ipv6_listen_options  => '', # disable default_server directive
    use_default_location => false,
    format_log           => 'lyslog',
    access_log           => '/var/log/nginx/userdir_access.log',
    error_log            => '/var/log/nginx/userdir_error.log',
    resolver             => [
      '[2001:6b0:17:f0a0::e1]',
      '130.236.254.225',
      '130.236.254.4',
    ],
    ssl                  => $tls_really_ready,
    ssl_redirect         => $tls_really_ready,
    ssl_cert             => $ssl_cert,
    ssl_key              => $ssl_key,
  }

  nginx::resource::location { "userdomain_test_location_${user}_${domain}":
    server         => "userdomain_server_${user}_${domain}",
    location       => '= /lysator-test-endpoint-do-not-use.txt',
    location_alias => '/var/validator_server/common_secret.txt',
    try_files      => ['$uri', '=404'],
    index_files    => [],
  }

  $fixed_location = squeeze("/${location}/", '/')

  if ($location[0,2] == '//') {
    $final_path = $fixed_location
  } else {
    $final_path = "/~${user}${fixed_location}"
  }


  nginx::resource::location { "userdomain_location_${user}_${domain}_${location}":
    server           => "userdomain_server_${user}_${domain}",
    location         => '/',
    proxy            => "http://www.lysator.liu.se:8080${final_path}\$request_uri",
    proxy_set_header => [ 'Host $host' ],
    proxy_redirect   => "http://\$host${final_path} /",
    ssl              => $tls_really_ready,
    ssl_only         => $tls_really_ready,
  }

}
