CREATE DATABASE IF NOT EXISTS wwwconf;

USE wwwconf;

CREATE TABLE IF NOT EXISTS userdomains (
    id INTEGER NOT NULL AUTO_INCREMENT,
    uid INTEGER NOT NULL,
    domain TEXT NOT NULL,
    location TEXT NOT NULL,
    tls_ready INTEGER NOT NULL DEFAULT 0,
    last_error TEXT,
    PRIMARY KEY ( id )
);

CREATE USER IF NOT EXISTS 'test'@'slartibartfast.lysator.liu.se' IDENTIFIED BY 'password';
GRANT ALL ON wwwconf.userdomains TO 'test'@'slartibartfast.lysator.liu.se';
