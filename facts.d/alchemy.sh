#!/usr/bin/env sh

# Exit silently if python3 is not available
/usr/bin/env python3 -c '' 2>/dev/null || exit 0

/usr/bin/env python3 <<'EOF'
# This fact is pulled onto all (?) hosts which has the "knuth" module
# in their environment (so just about all hosts). Qucik and dirty fix
# is to just eat all errors.
try:

    from sqlalchemy import create_engine
    from sqlalchemy import Column, Integer, String
    from sqlalchemy.orm import sessionmaker
    from sqlalchemy.ext.declarative import declarative_base

    import yaml
    import pwd
    import sys

    import traceback

    engine = create_engine('mysql://root@localhost/wwwconf')

    # But if we get this far we probably want to run, so an inner try
    # which doesn't eat the error
    try:
        Base = declarative_base()

        Session = sessionmaker(bind=engine)
        session = Session()

        class Userdomains(Base):
            __tablename__ = 'userdomains'
            id = Column(Integer, primary_key=True)
            uid = Column(Integer)
            domain = Column(String)
            location = Column(String)
            tls_ready = Column(Integer)
            last_error = Column(String)

        data = [ { 'uid': record.uid,
                   'user': pwd.getpwuid(record.uid).pw_name,
                   'domain': record.domain,
                   'location': record.location,
                   'tls_ready': record.tls_ready,
                   'last_error': record.last_error }
              for record in session.query(Userdomains) ]

        yaml.safe_dump({ 'userdomains': data }, stream=sys.stdout)
    except Exception as e:
        traceback.print_exc()

except:
    pass
EOF
